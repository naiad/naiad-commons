"""
@author: Jeff Piollé
"""
import logging
from typing import List, Tuple

import numpy as np

try:
    import matplotlib.pyplot as plt
except:
    logging.warning(
        'matplotlib, or cartopy are missing. Using display functions'
        ' will fail'
        )

import pyproj
import scipy.ndimage
import shapely.ops
import spherical_geometry.great_circle_arc as gc
import spherical_geometry.vector as vp
from scipy.cluster.hierarchy import fclusterdata
from shapely.geometry import (
    LinearRing,
    LineString,
    MultiLineString,
    MultiPoint,
    MultiPolygon,
    Point,
    Polygon,
    mapping,
    polygon,
)
from spherical_geometry.polygon import SphericalPolygon

NORTH_STEREO = ('+proj=stere +lat_ts=50. +lat_0=90 +lon_0=0. +k_0=1.0 +x_0=0'
                ' +y_0=0')
SOUTH_STEREO = ('+proj=stere +lat_ts=-50. +lat_0=-90 +lon_0=0. +k_0=1.0 +x_0=0'
                ' +y_0=0')

WORLD = Polygon([(-180., -90.), (180., -90), (180., 90.),
                 (-180., 90), (-180., -90.)])

WORLD_WEST = Polygon([(-540., -90.), (-180., -90), (-180., 90.),
                      (-540., 90), (-540., -90.)])
WORLD_EAST = Polygon([(180., -90.), (540., -90), (540., 90.),
                      (180., 90), (180., -90.)])
WORLD_WEST0 = Polygon([(-180., -90.), (0., -90), (0., 90.),
                       (-180., 90), (-180., -90.)])
WORLD_EAST0 = Polygon([(0., -90.), (180., -90), (180., 90.),
                       (0., 90), (0., -90.)])

# the spherical geometry library runs into assertions if using
# 90 or 180 exactly
epsilon = 1e-3
def EAST(e=epsilon):
    return SphericalPolygon.from_lonlat(
        lon=[0, 0, 180 - e, 180 - e],
        lat=[90 - e, -90 + e, -90 + e, 90 - e],
        center=(15, 0)
        )

def WEST(e=epsilon):
    return SphericalPolygon.from_lonlat(
        lon=[-180 + e, -180 + e, 0, 0],
        lat=[90 - e, -90 + e, -90 + e, 90 - e],
        center=(-15, 0)
    )

NORTH = (180, 90)
SOUTH = (180, -90)

DATELINE = np.dstack(
    vp.lonlat_to_vector(np.array([SOUTH[0], NORTH[0]]),
                        np.array([SOUTH[1], NORTH[1]]),
                        degrees=True)
)[0]


def estimate_resolution(lons, lats):
    a = vp.lonlat_to_vector(lons[0, 1:], lats[0, 1:])
    b = vp.lonlat_to_vector(np.roll(lons, 1, axis=1)[0, 1:],
                            np.roll(lats, 1, axis=1)[0, 1:])
    xres = gc.length(np.ma.array(a).transpose(), np.ma.array(b).transpose())

    a = vp.lonlat_to_vector(lons[1:, 0], lats[1:, 0])
    b = vp.lonlat_to_vector(np.roll(lons, 1, axis=0)[1:, 0],
                            np.roll(lats, 1, axis=0)[1:, 0])
    yres = gc.length(np.ma.array(a).transpose(), np.ma.array(b).transpose())
    return max(xres.max(), yres.max())


def sanitize(lons, lats, insider, resolution=None, check=False):
    """Returns a polygon constructed from a list of lat/lon vertices

    Transforms to spherical coordinates to fix pole and timeline issues.

    Args:
        slons: longitudes of polygon vertices
        slats: latitudes of polygon vertices
    """
    sp = SphericalPolygon.from_lonlat(
        lon=np.array(lons),
        lat=np.array(lats),
        center=insider)

    if insider is None:
        logging.warning('Insider not defined for tile of shape: {}'.format(
            np.array(lons).shape))

    if insider is not None and not sp.contains_lonlat(*(insider)):
        raise ValueError('Polygon is not valid. Should include insider point')

    geoms = []
    for cidx, clip_area in enumerate([WEST(), EAST()]):
        if not sp.intersects_poly(clip_area):
            continue

        clipped = list(sp.intersection(clip_area).to_lonlat())
        if len(clipped) == 0:
            # There should be a polygon here. SphericalPolygon sometimes don't
            # work => change the epsilon in the clip area definition
            if cidx == 0:
                clip_area = WEST(epsilon * 10.)
            else:
                clip_area = EAST(epsilon * 10.)
            clipped = list(sp.intersection(clip_area).to_lonlat())
            if len(clipped) == 0:
                logging.warning("Can't clip polygon {}".format(
                    list(sp.intersection(clip_area).to_lonlat())
                ))
                #raise ValueError("Can't clip polygon")

        # transform to regular polygon
        for val in clipped:
            plon, plat = val
            plon[plon > 180] -= 360
            poly = Polygon(np.array((plon, plat)).T).buffer(
                resolution, cap_style=3, resolution=0)

            # avoid union issues
            geoms.append(poly.intersection(WORLD))

    shape = orient(shapely.ops.unary_union(geoms))

    # check
    if insider is not None and not shape.contains(Point(insider)):
        if check:
            raise ValueError('shape does not contain point used as insider')
        else:
            logging.warning('shape does not contain point used as insider')

    return shape


def great_circle(lons, lats, resolution=None):
    if len(lons) != 2 or len(lats) != 2:
        raise ValueError('lon/lat must have two elements only')

    x, y, z = vp.lonlat_to_vector(lons, lats, degrees=True)
    arc = np.dstack((x, y, z))[0]

    # date line crossing
    crossing = gc.intersection(arc[0], arc[1], DATELINE[0], DATELINE[1])
    if np.isnan(crossing).sum() > 0:
        # no intersection
        line = LineString([(lons[0], lats[0]), (lons[1], lats[1])])

    else:
        crossp = vp.vector_to_lonlat(*crossing)
        line = MultiLineString([
            LineString([(lons[0], lats[0]),
                        (crossp[0] * np.sign(lons[0]), crossp[0])]),
            LineString([(lons[1], lats[1]),
                        (crossp[0] * np.sign(lons[0]), crossp[0])]),
        ])

    if resolution is not None and resolution != 0.:
        shape = orient(
            line.buffer(resolution, cap_style=3, resolution=1))
        return shape
    else:
        return line


def polyline(lons, lats, resolution=None):
    lines = [
        great_circle(lons[i:i+2], lats[i:i+2], resolution=resolution)
        for i, _ in enumerate(lons[:-1])
    ]
    return shapely.ops.unary_union(lines).simplify(0.01)


def _sp_to_lonlat(sp):
    res = []
    for val in sp.to_lonlat():
        plon, plat = val
        plon[plon > 180] -= 360
        res.append((plon, plat))
    return res


def convex_hull(lons, lats, resolution=None):
    """Returns a polygon constructed from a list of lat/lon as their complex
    hull

    Transforms to spherical coordinates to fix pole and timeline issues.

    Args:
        lons: longitudes of inside points
        lats: latitudes of inside points
    """
    # case where points along a meridian or parallel => convex hull does not
    # work well. Better use great arcs here.
    if len(np.unique(lons)) == 1 or len(np.unique(lats)) == 1:
        return polyline(lons, lats, resolution=resolution)

    x, y, z = vp.lonlat_to_vector(lons, lats, degrees=True)
    points = np.dstack((x, y, z))[0]

    # fix to avoid SphericalPolygon bug when not single extreme left point
    # in the hull
    j = np.argmin(np.arctan2(points[:, 1], points[:, 0]))
    extremes = np.count_nonzero(np.array(lons) == lons[j])
    if extremes > 1 and extremes != lons.size:
        lons[j] -= epsilon
        x, y, z = vp.lonlat_to_vector(lons, lats, degrees=True)
        points = np.dstack((x, y, z))[0]

    # get convex hull of the point cluster
    sp = SphericalPolygon.convex_hull(points)

    # case where points are aligned on a great circle (null area polygon)
    if sp.area() < epsilon:
        geoms = _sp_to_lonlat(sp)
        if len(geoms) != 1:
            raise ValueError('Great circle is not valid')
        plon, plat = geoms[0]
        return great_circle(plon[[0, -1]], plat[[0, -1]], resolution=resolution)

    geoms = []
    for cidx, clip_area in enumerate([WEST(), EAST()]):

        if not sp.intersects_poly(clip_area):
            continue
        #
        # for val in sp.to_lonlat():
        #     print("VAL ", val)
        #     plon, plat = val
        #     plon[plon > 180] -= 360
        #     coords2 = [(plon[i], plat[i]) for i, _ in enumerate(plon)]
        #     GeoShape.plot(coords2)

        clipped = _sp_to_lonlat(sp.intersection(clip_area))
        if len(clipped) == 0:
            # There should be a polygon here. SphericalPolygon sometimes don't
            # work => change the epsilon in the clip area definition
            if cidx == 0:
                clip_area = WEST(epsilon / 10.)
            else:
                clip_area = EAST(epsilon / 10.)
            clipped = _sp_to_lonlat(sp.intersection(clip_area))
            if len(clipped) == 0:
                logging.warning("Can't clip polygon")
                clipped = _sp_to_lonlat(sp)
                #raise ValueError("Can't clip polygon")

        # transform to regular polygon
        for lonlat in clipped:
            poly = Polygon(np.array(lonlat).T).buffer(
                resolution, cap_style=3, resolution=0)

            # avoid union issues
            geoms.append(poly.intersection(WORLD))

    return orient(shapely.ops.unary_union(geoms))


def valid_edges(data: 'np.array', crop=0) -> List[Tuple[slice]]:
    """return the subarray of data that contains valid values"""
    axes = len(data.shape)
    if crop != 0:
        mask = ~np.ma.getmaskarray(data)
        mask = scipy.ndimage.binary_erosion(
                mask, iterations=1, border_value=0)
        data = np.ma.masked_where(~mask, data)
    edges = []
    for axis in range(axes):
        axedges = np.ma.notmasked_edges(data, axis=axis)
        e0 = axedges[0][0].min()
        en = axedges[1][0].max() + 1
        edges.append(slice(e0, en))
    return edges


def subset_valid_latlon(lons, lats, crop=0):
    """return the subarray of lons and lats that contains valid values"""
    edges = valid_edges(lons, crop=crop)
    return lons[tuple(edges)], lats[tuple(edges)]


def get_inside_pixel(lons, lats):
    """return the lon, lat coordinates of a valid point within 2-dimensional
    arrays of longitudes and latitudes.
    """
    if len(lons.shape) != 2 or len(lats.shape) != len(lons.shape):
        raise ValueError(
            'longitudes and latitudes must be 2-dimensional and same size')

    lons, lats = subset_valid_latlon(lons, lats, crop=1)
    x, y = lons.shape
    if x < 3 or y < 3:
        logging.warning(
            'valid longitudes and latitudes must be more than 3 in each '
            'dimension.')
        #return lons.mean(), lats.mean(),
        return

    # return center pixel if valid
    xc, yc = x // 2, y // 2
    if ((-180. <= lons[xc, yc] <= 180.)
            and (-90. <= lats[xc, yc] <= 90.)):
        return lons[xc, yc], lats[xc, yc],

    # if not, get a valid point (lat or lon are masked)
    if lons[1:-1, 1:-1].count() == 0:
        raise ValueError('No valid lat/lon in data!')

    delta = 1
    while lons[xc-delta:xc+delta+1, yc-delta:yc+delta+1].count() == 0:
        delta = delta + 1

    clons = lons[xc-delta:xc+delta+1, yc-delta:yc+delta+1]
    clats = lats[xc-delta:xc+delta+1, yc-delta:yc+delta+1]
    valids = np.nonzero(~clons.mask)
    mid = len(valids[0]) // 2
    xc, yc = valids[0][mid], valids[1][mid]

    return clons[xc, yc], clats[xc, yc],


def orient(shape):
    """Make a shape definition counterclockwise.

    The use of shapely unary_union or cascaded_union creates clockwise
    polygons within the created multipolygon, this needs to be fixed for
    the shapes to be correctly indexed.

    Args:
        shape : a shapely geometry object
    """
    if isinstance(shape, MultiPolygon):
        return MultiPolygon([orient(_) for _ in shape.geoms])
    elif isinstance(shape, Polygon):
        return polygon.orient(shape)
    else:
        return shape


class GeoShape(object):
    """
    Class for handling geographical shapes over a cartesian representation of
    the globe.

    The coordinates of a polygonal shape must be expressed counterclockwise.
    Clockwise coordinates are assumed to be crossing the date line (meridian
    180).
    """
    @staticmethod
    def close(coords):
        """close polygon vertices
        """
        closedcoords = coords
        # close polygon
        if coords[-1] != coords[0]:
            closedcoords.append(coords[0])
        return closedcoords

    @staticmethod
    def geojson(shape):
        """
        Return the shape as a geojson object

        Args:
            shape (shapely.geometry): a shapely geometry inherited object
        """
        geojson_area = mapping(shape)
        geojson_area['type'] = geojson_area['type'].lower()
        if (geojson_area['type'] in ['linestring', 'multilinestring'] and
                'orientation' in geojson_area):
            del geojson_area['orientation']
        #else:
        #    geojson_area["orientation"] = "counterclockwise"
        return geojson_area

    @staticmethod
    def polyline(lons, lats) -> shapely.geometry.base:
        """Initialize a polyline from a list of lat/lon.
        """
        if lats.count() == 0:
            # @TODO returns Empty Geometry
            return

        # @TODO   PolyLine instead
        shape = LineString([(lons[0], lats[0]), (lons[-1], lats[-1])])

        # detect dateline crossing
        if lons[0] * lons[-1] < 0 and shape.length > 180.:
            validshape = GeoShape.unwrap_cylindric_shape(shape)
            if validshape is None:
                return None
            # split shapes overlapping the dateline
            shapes = GeoShape.split_over_dateline(validshape)
            if len(shapes) > 1:
                if isinstance(shapes[0], LineString):
                    shapes = MultiLineString(shapes)
                else:
                    shapes = GeoShape.orient(shapely.ops.unary_union(shapes))
            return shapes
        else:
            return shape

    @staticmethod
    def get_shape_from_clusters(lons, lats, gap_threshold=10.):
        """Process the shape from clusters of points.

        The shape will be a multipolygon if point clusters are
        separated from more than 10 degrees (on a cartesian map).
        """
        data = np.ma.column_stack((lons, lats))
        clusters = fclusterdata(data,
                                gap_threshold,
                                criterion='distance')
        shapes = [polygon.orient(
                    MultiPoint(
                        list(zip(lons[clusters == _],
                                 lats[clusters == _]))).convex_hull)
                  for _ in range(min(clusters), max(clusters)+1)
                  if ((clusters == _).sum() > 2)
                  ]
        shape_union = shapely.ops.unary_union(shapes)
        return GeoShape.orient(shape_union)


    @staticmethod
    def polygon(lons, lats,
                vertices='corners',
                use_clustering=False,
                resolution=None,
                gap_threshold=10.,
                check=False,
                **kwargs):
        """Initialize a polygon from a 2 dimensional arrays of lat/lon.

        Two types of vertices are proposed to define the polygonal shape:
            * "corners": use the corners of the lat/lon arrays. This will define
                a quadrangle. Adapted to grids or swaths with straight geometry
                and fast. Possible issues at poles and date line crossing.
            * "convex_hull": adjust the polygon to the smallest convex Polygon
                containing all the points in the matrix. More precise for curvy
                geometries. Likely to result in disjoint polygons when tiling a
                full swath this way.
        """
        if lats.count() == 0:
            logging.debug('No valid lat/lon in this tile')
            return

        if resolution is None:
            resolution = estimate_resolution(lons, lats)

        if (vertices == 'convex_hull' or
                (np.ma.is_masked(lats[0, :]) or
                 np.ma.is_masked(lats[-1, :]) or
                 np.ma.is_masked(lats[:, 0]) or
                 np.ma.is_masked(lats[:, -1]))):
            if vertices != 'convex_hull':
                logging.debug(
                    'Applying convex hull for a tile with invalid values')

            # method 2 : convex hull
            # used by default when invalid lat/lon are present
            #if tlons.count() <= 2:
            #    logging.warning("Only two valid points for a tile: skipped.")
            #    return None
            if use_clustering:
                # case when there is a cut in the swath section and/or the
                # swath is filled with holes
                shapes = GeoShape.get_shape_from_clusters(
                    lons.compressed(), lats.compressed(), gap_threshold)
                return shapes
            else:
                if len(lons.compressed()) > 3:
                    return convex_hull(
                        lons.compressed(), lats.compressed(),
                        resolution=resolution)

                shape = orient(MultiPoint(
                    list(zip(lons.compressed(), lats.compressed()))
                ).convex_hull)

                if isinstance(shape, (Point, LineString)):
                    # transform to polygon
                    insider = tuple(shape.representative_point().coords[0])
                else:
                    #insider = tuple(shape.representative_point().coords[0])
                    try:
                        insider = get_inside_pixel(lons, lats)
                    except ValueError:
                        insider = tuple(shape.representative_point().coords[0])

                shape = orient(
                    shape.buffer(resolution, cap_style=3, resolution=1))
                resolution = 0

                # extend coverage and clean cut over date line and poles
                slons, slats = shape.exterior.xy
                return sanitize(
                    np.ma.array(slons),
                    np.ma.array(slats),
                    insider,
                    resolution=resolution,
                    check=check)

                # dilate shape to account for pixel resolution
                shape = orient(
                    shape.buffer(resolution, cap_style=3, resolution=1))

                # orient counterclockwise for valid polygons
                if isinstance(shape, (Polygon, MultiPolygon)):
                    shape = polygon.orient(shape)

                # split if crossing date line
                shape = GeoShape.split_over_dateline(shape)
                # case where there is a probable date line overlap
                # use cluster method instead
                #if shape.length > 360.:
                #    shape = GeoShape.get_shape_from_clusters(
                #        tlons.compressed(), tlats.compressed(), gap_threshold
                #    )
                return shape

        elif vertices == 'corners':
            slons = [lons[0, 0], lons[0, -1], lons[-1, -1], lons[-1, 0]]
            slats = [lats[0, 0], lats[0, -1], lats[-1, -1], lats[-1, 0]]
            insider = get_inside_pixel(lons, lats)

            return sanitize(slons, slats, insider, resolution, check=check)

        else:
            raise ValueError('Unknown method: {}'.format(vertices))

    @staticmethod
    def get_valid_shapes(shape):
        """reform a geographical shape into valid shape over a cartesian latlong
         world representation.

        Possible reasons for badly shaped polygons or lines are : crossing date
        line, overlapping north or south pole, or being to close to one of these
        poles.

        Such shapes are broken down in sub-elements (MultiPolygon or
        MultiLine).

        """
        # case overlaping pole
        if GeoShape.is_pole_in(shape):
            # pole crossing
            logging.debug('Pole crossing processing case')
            validshape = GeoShape.unwrap_polar_shape(shape)
            return [polygon.orient(Polygon(validshape.coords))]
        elif not shape.is_simple:
            # case close to pole
            if not shape.is_simple:
                logging.debug('Close to pole processing case')
                shapes = GeoShape.uncross(shape)
                return shapes
        elif not shape.exterior.is_ccw or shape.length > 360.:
            # issue with dateline
            logging.debug('Date line crossing processing case')
            validshape = GeoShape.unwrap_cylindric_shape(shape)
            if validshape is None:
                return None
            # split shapes overlapping the dateline
            return GeoShape.split_over_dateline(validshape)
        else:
            return [polygon.orient(shape)]

    @staticmethod
    def get_point(shape, indice):
        return shape.exterior.coords[indice]

    @staticmethod
    def get_lat(shape, indice):
        return GeoShape.get_point(shape, indice)[1]

    @staticmethod
    def count(shape):
        return len(shape.coords)

    @staticmethod
    def uncross(shape):
        """Create a polygon on latlong projection from a polygon
        overlapping north or south poles.
        """
        # assume polygone fully contained in one single hemisphere
        p1 = pyproj.Proj(proj='latlong', datum='WGS84')
        # check hemisphere (assumed fully contained in a single hemishere)
        if GeoShape.get_lat(shape, 0) > 0.:
            p2 = pyproj.Proj(NORTH_STEREO)
        else:
            p2 = pyproj.Proj(SOUTH_STEREO)
        new_coords = []
        # reproject in stereographic
        for point in shape.coords:
            lon, lat = point
            x, y = pyproj.transform(p1, p2, lon, lat)
            new_coords.append((x, y))
        stereo_polygon = Polygon(new_coords)
        x, y = GeoShape.xy(stereo_polygon)
        # intersection with 0/dateline meridian
        west = Polygon([(0, min(y)),
                       (min(x), min(y)),
                       (min(x), max(y)),
                       (0, max(y)),
                       (0, min(y))])
        east = Polygon([(0, min(y)),
                       (max(x), min(y)),
                       (max(x), max(y)),
                       (0, max(y)),
                       (0, min(y))])
        intersection1 = stereo_polygon.intersection(west)
        intersection2 = stereo_polygon.intersection(east)

        # reproject each intersection in latlong
        coords = []
        for x, y in intersection1.exterior.coords:
            lon, lat = pyproj.transform(p2, p1, x, y)
            lon = lon - 360. if lon > 0. else lon
            coords.append((lon, lat))
        westpol = polygon.orient(Polygon(coords))
        coords = []
        for x, y in intersection2.exterior.coords:
            lon, lat = pyproj.transform(p2, p1, x, y)
            lon = lon + 360. if lon < 0. else lon
            coords.append((lon, lat))
        eastpol = polygon.orient(Polygon(coords))
        return [eastpol, westpol]

    @staticmethod
    def plot(coords):
        vert = list(zip(*coords))
        plt.plot(list(vert[0]), list(vert[1]))
        plt.show()

    @staticmethod
    def is_pole_in(shape):
        """Return True if north or south pole included in the polygon.

        Assume the coordinates were defined counterclockwise.
        """
        p1 = pyproj.Proj(proj='latlong',datum='WGS84')
        # check hemisphere (assumed fully contained in a single hemishere)
        if GeoShape.get_lat(shape, 0) > 0.:
            p2 = pyproj.Proj('+proj=stere +lat_ts=50. +lat_0=90 +lon_0=0. +k_0=1.0 +x_0=0 +y_0=0')
            pole = (0., 90.)
        else:
            p2 = pyproj.Proj('+proj=stere +lat_ts=-50. +lat_0=-90 +lon_0=0. +k_0=1.0 +x_0=0 +y_0=0')
            pole = (0., -90.)
        new_coords = []
        # reproject in stereographic
        for point in shape.exterior.coords:
            lon, lat = point
            x, y = pyproj.transform(p1, p2, lon, lat)
            new_coords.append((x, y))
        stereo_polygon = Polygon(new_coords)
        pole = Point(pyproj.transform(p1, p2, pole[0], pole[1]))
        #GeoShape.plot(stereo_polygon.exterior.coords)
        return stereo_polygon.contains(pole)

    @staticmethod
    def xy(shape):
        if isinstance(shape, Polygon):
            x, y = shape.exterior.xy
        elif isinstance(shape, LineString):
            x, y = shape.xy
        return np.array(x, copy=False), np.array(y, copy=False)

    @staticmethod
    def unwrap_cylindric_shape(shape):
        """shift the longitudes of a shape crossing the dateline to remove
        discontinuities
        """
        lons, lats = GeoShape.xy(shape)
        shapeclass = shape.__class__

        #GeoShape.plot(shape.exterior.coords)
        if min(lons) * max(lons) > 0:
            # in some products, clockwise order may be reversed at some point!
            # we first try to check this
            if shape.within(WORLD):
                pol = polygon.orient(shapeclass(list(zip(lons, lats))))
                if pol.is_valid:
                    return pol
            logging.error('This is not a dateline crossing. Investigate')
            return None
        lons[lons < 0] += 360.
        return shapeclass(list(zip(lons, lats)))

    @staticmethod
    def unwrap_polar_shape(shape):
        """Create a polygon on latlong projection from a polygon
        overlapping north or south poles.
        """
        if not isinstance(shape, Polygon):
            raise ValueError('This methods is only valid for polygons')
        # assume polygon fully contained in one single hemisphere
        pole_lat = 90. if GeoShape.get_lat(shape, 0) > 0 else -90.
        dtype = [('lon', float), ('lat', float)]
        values = np.array(shape.exterior.coords[:-1], dtype=dtype)
        # order eastward
        coords = np.sort(values, order='lon')[::-1].tolist()
        # duplicate most eastward and westward points shifted beyond
        # [-180, 180] range and close polygon by pole
        easternmost = coords[0]
        westernmost = coords[-1]
        coords.extend([
            (easternmost[0] - 360, easternmost[1]),
            (easternmost[0] - 360, pole_lat),
            (westernmost[0] + 360, pole_lat),
            (westernmost[0] + 360, westernmost[1]),
            coords[0]
            ])
        # clip
        unwrapped_polygon = polygon.orient(WORLD.intersection(Polygon(coords)))
        return LinearRing(unwrapped_polygon.exterior.coords)

    @staticmethod
    def split_over_greenwich(shape):
        """Split a shape along meridian 0 and returns a multipolygon instead.

        Assume the initial shape is valid and within -180/-90/180/90 limits,
        unwrapped over pole and dateline.

        Args:
            shape (Polygon, MultiPolygon): a shapely geometry object.

        Returns:
            MultiPolygon: a shapely multipolygon matching the original shape
            area.

        """
        splittiles = []
        if shape.geom_type == 'MultiPolygon':
            shapes = shape.geoms
        else:
            shapes = [shape]

        for pol in shapes:
            # fix some small polygon issues resulting in a
            # GeometryCollection when intersecting below
            #pol = pol.buffer(0.01)

            if pol.intersects(LineString([(0., -90.), (0, 90.)])):
                tilewest = pol.intersection(WORLD_WEST0)
                if tilewest.geom_type == 'GeometryCollection':
                    logging.error('Wrong intersection with globe map. This is'
                                  'likely caused by a weird polygon case.'
                                  'Contact the developer.')
                    for geom in tilewest.geoms:
                        GeoShape.draw(geom)
                        plt.show()
                    raise Exception('Bad intersection result')
                if not tilewest.is_empty:
                    if tilewest.geom_type != 'MultiPolygon':
                        splittiles.append(tilewest)
                    else:
                        splittiles.extend(tilewest.geoms)

                tileeast = pol.intersection(WORLD_EAST0)
                if tileeast.geom_type == 'GeometryCollection':
                    logging.error('Wrong intersection with globe map. This is'
                                  'likely caused by a weird polygon case.'
                                  'Contact the developer.')
                    for geom in tileeast.geoms:
                        GeoShape.draw(geom)
                        plt.show()
                    raise Exception('Bad intersection result')
                if not tileeast.is_empty:
                    if tileeast.geom_type != 'MultiPolygon':
                        splittiles.append(tileeast)
                    else:
                        splittiles.extend(tileeast.geoms)
            else:
                splittiles.append(pol)
        # re-unite all polygons in a multipolygon
        if len(splittiles) > 1:
            return MultiPolygon(splittiles)
        else:
            return splittiles[0]


    @staticmethod
    def split_over_dateline(shape):
        """Split a shape overlapping the date line"""
        shapeclass = shape.__class__
        if WORLD.contains(shape):
            return [shape]
        splited_shapes = []
        try:
            subshape = WORLD.intersection(shape)
        except:
            logging.error('Tiling error with shape {}'.format(shape))
            return []
        if not subshape.is_empty:
            splited_shapes.append(subshape)
        subshape = WORLD_EAST.intersection(shape)
        if not subshape.is_empty and not np.isclose(subshape.length, 0.0):
            #GeoShape.plot(shape.exterior.coords)
            #plt.show
            lons, lats = GeoShape.xy(subshape)
            lons -= 360.
            subshape = shapeclass(list(zip(lons, lats)))
            if isinstance(subshape, Polygon):
                subshape = polygon.orient(subshape)
            splited_shapes.append(subshape)
        subshape = WORLD_WEST.intersection(shape)
        if not subshape.is_empty and not np.isclose(subshape.length, 0.0):
            lons, lats = GeoShape.xy(subshape)
            lons += 360.
            subshape = shapeclass(list(zip(lons, lats)))
            if isinstance(subshape, Polygon):
                subshape = polygon.orient(subshape)
            splited_shapes.append(subshape)
        if len(splited_shapes) > 3 or len(splited_shapes) == 0:
            raise Exception('Problem...')
        return splited_shapes
