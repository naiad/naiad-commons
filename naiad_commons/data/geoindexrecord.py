"""
Class to store a feature index record
"""
import hashlib
import logging
import typing as T
from ast import literal_eval
from collections import OrderedDict
from datetime import datetime
from pathlib import Path

import numpy as np
import shapely.geometry
import shapely.ops
import shapely.wkt
from dateutil import parser

try:
    import cartopy.crs as ccrs
    import cartopy.feature as cfeature
    import matplotlib.pyplot as plt
except:
    logging.warning(
        'matplotlib, or cartopy are missing. Using display functions'
        ' will fail'
    )


def read_csv(filepath, granule_only=False):
    """open tile file and read records"""
    with open(filepath, 'r') as fp:
        georecord = FeatureIndexRecord.decode_csv(fp.readline())

        # check the footprint shape is valid
        if not georecord.geometry.is_valid:
            logging.warning('invalid granule in {}. skipped.'.format(filepath))
            logging.debug(str(georecord))
            return

        if granule_only:
            return georecord

        segments = []
        for line in fp:
            seg = SegmentIndexRecord.decode_csv(line)
            if seg.geometry.is_valid:
                segments.append(seg)
            else:
                logging.warning('an invalid segment was skipped.')
                logging.debug(str(seg))

            # correct clockwise multipolygons
            # tile.geometry = orient(tile.geometry)

        georecord.segments = segments

    return georecord


class SegmentIndexRecord(object):
    """Properties of feature segment"""
    def __init__(
            self,
            granule: str,
            feature: str,
            dataset_id: str = None,
            geometry: shapely.geometry.base.BaseGeometry = None,
            start: T.Union[datetime, np.datetime64] = None,
            end: T.Union[datetime, np.datetime64] = None,
            slices: T.Dict[str, slice] = None,
            properties: T.Dict[str, T.Any] = None):

        if slices is None:
            slices = {}

        if properties is None:
            properties = {}

        if geometry is not None and not isinstance(
                geometry, shapely.geometry.base.BaseGeometry):
            raise TypeError('geometry is not a shapely object.')

        if not isinstance(slices, dict):
            raise TypeError('slices must be an OrderedDict')

        if not isinstance(properties, dict):
            raise TypeError('properties must be an OrderedDict')

        if isinstance(geometry, shapely.geometry.MultiPolygon):
            geometry = shapely.ops.unary_union([
                _.buffer(0.) for _ in geometry.geoms])
            # geometry.show()

        self.geometry = geometry

        # times
        self.start = start
        if isinstance(start, np.datetime64):
            self.start = datetime.utcfromtimestamp(
                start.astype('O')/1e9)

        self.end = end
        if isinstance(start, np.datetime64):
            self.end = datetime.utcfromtimestamp(
                end.astype('O')/1e9)

        self.properties = properties
        self.slices = slices

        self.granule = granule
        self.dataset_id = dataset_id
        self.feature = feature

    def __str__(self):
        strtile = \
            f'Start time: {self.start}\n' \
            f'End time  : {self.end}\n' \
            f'Geometry  : {self.geometry}\n'

        if self.slices is not None:
            strtile += f'Slices    : {self.slices}\n'

        if self.properties is not None:
            strtile += f'Properties: {self.properties}\n'

        return strtile

    @classmethod
    def decode_csv(cls, line):
        line = line.replace(' nan,', ' None,')
        fields = line.split(';')
        if len(fields) == 5:
            raise ValueError(f'Not a Tile object. Missing fields in {fields}')

        # mandatory GeoRecord fields
        granule, startstr, endstr, polygon, feature = fields[0:5]
        rec = SegmentIndexRecord(
            granule=granule,
            feature=feature,
            geometry=shapely.wkt.loads(polygon),
            start=parser.parse(startstr),
            end=parser.parse(endstr),
        )

        # manage old and new format
        idx = 5
        if type(literal_eval(fields[idx])) is not list:
            #  old format for slices
            slices = []
            while (idx < len(fields)
                   and type(literal_eval(fields[idx])) is not list):
                slices.append(int(fields[idx]))
                idx += 1
            rec.slices = cls._slices_from_csv(slices, feature)
        else:
            # in new format, next items should be slices and properties lists of
            # tuples
            rec.slices = OrderedDict(literal_eval(fields[idx]))
            idx += 1

        if idx == len(fields):
            return rec

        # properties
        rec.properties = OrderedDict(literal_eval(fields[idx]))

        return rec

    @staticmethod
    def _slices_from_csv(offsets, feature):
        """Return tile offsets as slices"""
        if feature == 'Trajectory':
            return OrderedDict([('time', slice(offsets[0], offsets[1]))])
        elif feature == 'Swath':
            return OrderedDict([
                ('row', slice(offsets[0], offsets[1])),
                ('cell', slice(offsets[2], offsets[3]))])
        elif feature == 'Grid':
            return OrderedDict([
                ('y', slice(offsets[0], offsets[1])),
                ('x', slice(offsets[2], offsets[3]))])
        elif feature == 'CylindricalGrid':
            return OrderedDict([
                ('lat', slice(offsets[0], offsets[1])),
                ('lon', slice(offsets[2], offsets[3]))])

    @classmethod
    def from_geojson(cls, obj: dict):
        return SegmentIndexRecord(
            granule=obj['granule'],
            feature=obj['feature'],
            dataset_id=obj['dataset_id'],
            geometry=shapely.geometry.shape(obj['geometry']),
            start=datetime.strptime(
                obj['time_coverage_start'], '%Y-%m-%dT%H:%M:%S'),
            end=datetime.strptime(
                obj['time_coverage_end'], '%Y-%m-%dT%H:%M:%S'),
            slices=obj['slices'],
            properties=obj['properties'],
        )

    def geojson(self):
        """return the tile shape as a geojson python object"""
        geojson_area = shapely.geometry.mapping(self.geometry)
        geojson = {
            'granule': self.granule,
            'feature': self.feature,
            'dataset_id': self.dataset_id,
            'geometry': geojson_area,
            'time_coverage_start': self.start.isoformat(),
            'time_coverage_end': self.end.isoformat(),
            'properties': self.properties,
        }

        if self.slices is not None:
            geojson['slices'] = {
                k: [v.start, v.stop] for k, v in self.slices.items()
            }

        return geojson

    def hash(self, granule):
        """return a  unique hash code identifying the tile"""
        m = hashlib.md5()
        m.update(granule.encode('utf-8'))
        for sl in self.slices.values():
            m.update(str(sl.start).encode('utf-8'))
            m.update(str(sl.stop).encode('utf-8'))

        return m.hexdigest()

    @classmethod
    def draw(cls, geometry, colour='blue', ax=None):
        if ax is None:
            figure = plt.figure(figsize=(20, 20), constrained_layout=False)
            ax = figure.add_subplot(projection=ccrs.PlateCarree())
            land_50m = cfeature.NaturalEarthFeature(
                'physical', 'land', '50m',
                edgecolor='face', facecolor=cfeature.COLORS['land'])
            ax.add_feature(land_50m)
            ax.coastlines('50m')
            gl = ax.gridlines(draw_labels=True)
            gl.top_labels = False
            gl.left_labels = False

        lines = (shapely.geometry.LineString, shapely.geometry.MultiLineString)
        points = (shapely.geometry.Point, shapely.geometry.MultiPoint)
        if isinstance(geometry, lines):
            ax.add_geometries(
                [geometry], crs=ccrs.PlateCarree(),
                alpha=0.2, facecolor='none', edgecolor=colour, zorder=2
            )
        elif isinstance(geometry, points):
            ax.scatter(geometry.x, geometry.y, marker='+', color=colour)
        else:
            ax.add_geometries(
                [geometry], crs=ccrs.PlateCarree(),
                alpha=0.2, facecolor=colour, edgecolor=colour, zorder=2
            )

        return ax


class FeatureIndexRecord(SegmentIndexRecord):
    """Class to store an observation feature.

    @TODO remove slices in GeoRecord

    Args:
        geometry (BaseGeometry): shape of the tile. Must be provided as a
            shapely object (Polygon, MultiPolygon, MultiLine).

    """
    def __init__(
            self,
            granule: str,
            feature: str,
            dataset_id: str = None,
            path: Path = None,
            geometry: shapely.geometry.base.BaseGeometry = None,
            start: T.Union[datetime, np.datetime64] = None,
            end: T.Union[datetime, np.datetime64] = None,
            slices: T.Dict[str, slice] = None,
            properties: T.Dict[str, T.Any] = None,
            segments: T.List[SegmentIndexRecord] = None,
    ):
        super(FeatureIndexRecord, self).__init__(
            granule=granule, feature=feature, dataset_id=dataset_id,
            geometry=geometry, start=start, end=end, properties=properties,
            slices=slices
        )

        self.path = path
        self.segments = segments

    def __str__(self):
        strtile = \
            f'Name      : {self.granule}\n' \
            f'Dataset   : {self.dataset_id}\n'
        if self.feature is not None:
            strtile += f'Feature   : {self.feature}\n'
        if self.path is not None:
            strtile += f'Path      : {self.path}\n'
        strtile += super(FeatureIndexRecord, self).__str__()

        if self.segments is not None:
            for i, tile in enumerate(self.segments):
                strtile += f'\nSubset [{i}]\n' + tile.__str__()

        strtile += '\n'

        return strtile

    def geojson(self):
        """return the tile shape as a geojson python object"""
        geojson = super(FeatureIndexRecord, self).geojson()
        geojson.update({
            'granule': self.granule,
            'feature': self.feature,
        })

        if self.segments is not None:
            geojson['subsets'] = [_.geojson() for _ in self.segments]

        return geojson

    def encode_attrs_csv(
            self,
            granule: str,
            feature_type: str,
            start: datetime,
            end: datetime,
            geometry: shapely.geometry.base,
            slices: T.Dict[str, slice],
            properties: T.Dict[str, T.Union[str, datetime]]

    ):
        """Encode into a csv line"""
        line = ';'.join([
            str(granule),
            start.strftime('%Y-%m-%dT%H:%M:%S'),
            end.strftime('%Y-%m-%dT%H:%M:%S'),
            str(geometry),
            feature_type if feature_type is not None else ''
        ])

        # slices
        if slices is not None:
            for k, v in slices.items():
                line += ';' + ';'.join([str(v.start), str(v.stop)])

        # properties
        if properties is not None and len(properties) > 0:
            properties = {}
            for prop, value in self.properties.items():
                if isinstance(value, datetime):
                    properties[prop] = value.strftime('%Y-%m-%dT%H:%M:%S')
                else:
                    properties[prop] = value
            line += ';%s' % properties

        line += '\n'

        return line

    def encode_csv(self):
        """Encode a FeatureIndexRecord into a csv multiline"""
        line = self.encode_attrs_csv(
            self.granule, self.feature, self.start, self.end,
            self.geometry, self.slices, self.properties)

        if self.segments is None:
            return line

        for seg in self.segments:
            line += self.encode_attrs_csv(
                self.granule, self.feature, seg.start, seg.end,
                seg.geometry, seg.slices, seg.properties)

        return line

    def to_csv(self, output_dir: Path, organize_per_time: bool = False) -> Path:
        """Save as a CSV file (Naiad old format)"""
        fullpath = Path(output_dir)
        if organize_per_time:
            # archive in year/day subfolder structure
            fullpath = output_dir / Path(self.start.strftime('%Y/%j'))
        fullpath.mkdir(parents=True, exist_ok=True)

        outfname = fullpath / (self.granule + '.idx')

        with open(outfname, 'w') as outfile:
            outfile.write(self.encode_csv())

        return outfname
    @classmethod
    def from_geojson(cls, obj: dict):
        print(obj['slices'])

        return FeatureIndexRecord(
            granule=obj['granule'],
            feature=obj['feature'],
            geometry=shapely.geometry.shape(obj['geometry']),
            start=datetime.strptime(
                obj['time_coverage_start'], '%Y-%m-%dT%H:%M:%S'),
            end=datetime.strptime(
                obj['time_coverage_end'], '%Y-%m-%dT%H:%M:%S'),
            segments=[
                SegmentIndexRecord.from_geojson(_) for _ in obj['subsets']]
        )

    @classmethod
    def decode_csv(cls, line):
        line = line.replace(' nan,', ' None,')
        fields = line.split(';')

        # mandatory GeoRecord fields
        granule, startstr, endstr, polygon, feature = fields[0:5]
        rec = FeatureIndexRecord(
            granule.strip(),
            feature=feature.strip(),
            geometry=shapely.wkt.loads(polygon),
            start=parser.parse(startstr),
            end=parser.parse(endstr),
        )

        if len(fields) > 5:
            # properties
            rec.properties = OrderedDict(literal_eval(fields[5]))

        return rec


    def show(self,
             show_segments: bool = False,
             clip_area: shapely.geometry.Polygon = None):
        """Draw the feature index record on a map.

        Args:
            clip_area: a polygon shape corresponding to the selection area of
                a granule footprint to be shown in image background.
        """
        ax = self.draw(self.geometry, colour='green')

        if show_segments:
            for seg in self.segments:
                seg.draw(seg.geometry, colour='green', ax=ax)

        if clip_area:
            ax.draw(clip_area, colour='red', ax=ax)

        plt.show()


class Crossover(object):
    """Class for storing a crossover"""
    def __init__(
            self,
            reference: FeatureIndexRecord,
            match: FeatureIndexRecord,
            intersection: shapely.geometry.base = None):
        self.reference = reference
        self.match = match
        self.intersection = intersection

    def geojson(self):
        """return the crossover as a geojson python object"""
        return [self.reference.geojson(), self.match.geojson(),
                self.intersection]

    @classmethod
    def from_geojson(cls, obj: T.List):
        print(obj)
        if len(obj) != 3:
            raise ValueError('A crossover should have only two matched '
                             'features')

        return (FeatureIndexRecord.from_geojson(obj[0]),
                FeatureIndexRecord.from_geojson(obj[1]),
                obj[2])
